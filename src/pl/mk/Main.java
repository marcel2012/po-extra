package pl.mk;
import java.util.ArrayList;
import java.util.Scanner;
public class Main {
    private static class Complex{
        private double a, b;
        Complex(double a){ this.a=a; b=0; }
        Complex(double a, double b){ this.a=a; this.b=b; }
        Complex add(Complex x){ return new Complex(a+x.a, b+x.b); }
        Complex multiply(Complex x){
            return new Complex((a*x.a)-(b*x.b), (a*x.b)+(b*x.a));
        }
        Complex reciprocal(){
            return new Complex(a/((a*a)+(b*b)),-b/((a*a)+(b*b)));
        }
        double getModule(){ return Math.sqrt((a*a)+(b*b)); }
    }
    private abstract static class Element{
        private Complex Z;
        Element(Complex Z){ this.Z=Z; }
        Complex getZ(){ return Z; }
    }
    private static class Resistor extends Element{
        Resistor(double r){
            super(new Complex(r));
        }
    }
    private static class Inductor extends Element{
        Inductor(double f, double l){
            super(new Complex(0.0,2*Math.PI*f*l));
        }
    }
    private static class Capacitor extends Element{
        Capacitor(double f, double c){
            super((new Complex(0.0,2*Math.PI*f*c)).reciprocal());
        }
    }
    private static class MatrixRepr {
        private Complex dat[][];
        private MatrixRepr(Complex a11, Complex a12, Complex a21, Complex a22){
            dat=new Complex[][]{{a11,a12},{a21,a22}};
        }
        Complex getData(int w, int k){ return dat[w][k]; }
        MatrixRepr multiply(MatrixRepr m){
            Complex a=dat[0][0],b=dat[0][1],c=dat[1][0],d=dat[1][1];
            Complex x=m.dat[0][0],y=m.dat[0][1],z=m.dat[1][0],t=m.dat[1][1];
            return new MatrixRepr(a.multiply(x).add(b.multiply(z)),
                    a.multiply(y).add(b.multiply(t)),
                    c.multiply(x).add(d.multiply(z)),
                    c.multiply(y).add(d.multiply(t)));
        }
        static MatrixRepr ParallelConnector (Element elem) {
            return new MatrixRepr(new Complex(1), new Complex(0),
                                  elem.getZ().reciprocal(), new Complex(1));
        }
        static MatrixRepr SeriesConnector (Element elem) {
            return new MatrixRepr(new Complex(1), elem.getZ(),
                                  new Complex(0), new Complex(1));
        }
    }
    public static void main(String... in) {
        if(in.length<3) throw new NullPointerException();
        double min=Double.parseDouble(in[0]),
               step=Double.parseDouble(in[1]),
               max=Double.parseDouble(in[2]);
        ArrayList<String> attr=new ArrayList<>();
        for(Scanner s=new Scanner(System.in);s.hasNext();attr.add(s.next()));
        for(double freq=min; freq <= max; freq += step) {
            MatrixRepr circuit=MatrixRepr.SeriesConnector(new Resistor(0));
            for(String s : attr) {
                if(s.length()<4)
                    throw new NullPointerException();
                Element elm;
                MatrixRepr connect;
                double param=Double.parseDouble(s.substring(3));
                switch (s.charAt(1)){
                    case 'R': elm=new Resistor(param); break;
                    case 'C': elm=new Capacitor(freq, param); break;
                    case 'L': elm=new Inductor(freq, param); break;
                    default: continue;
                }
                switch (s.charAt(0)){
                    case '+': connect=MatrixRepr.SeriesConnector(elm); break;
                    case '-': connect=MatrixRepr.ParallelConnector(elm); break;
                    default: continue;
                }
                circuit=circuit.multiply(connect);
            }
            Complex a11=circuit.getData(0,0);
            double Z=a11.multiply(circuit.getData(1,0)
                    .reciprocal()).getModule();
            System.out.println(freq+" "+a11.reciprocal().getModule()+" "+Z);
        }
    }
}